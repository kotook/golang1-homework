package main

import (
	"fmt"
	"homework8/configuration"
)
func main() {
	conf := configuration.Configuration{}
	conf.Load()
	fmt.Print(conf)
}